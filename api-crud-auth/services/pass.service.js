const bcrypt = require('bcrypt');

function encriptaPassword(password)
{
    return bcrypt.hash(password, 10);
}

// Devuelve truee o false
function comparaPassword(password, hash)
{
    return bcrypt.compare(password, hash);
}

module.exports = {
    encriptaPassword,
    comparaPassword
}