'use strict'

const port = process.env.PORT || 4400;

const https = require('https');
const fs = require('fs');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const { Certificate } = require('crypto');
const Password = require('./services/pass.service');
const Token = require('./services/token.service');
const moment = require('moment');

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const URL_DB = "mongodb+srv://dmg102:BYEfqRO1gq4Aeh7F@sd.c6nun.mongodb.net/users?retryWrites=true&w=majority";

const app = express();

var db = mongojs(URL_DB); //Enlace con la DB
var id = mongojs.ObjectID; //Convertir un id textual en objeto mongojs

//Declaramos nuestros middleWare
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) =>{
    console.log('param/api/:colecciones');
    console.log('coleccion: ', coleccion);

    //Creamos puntero a una funcion que apunta a la DB y tabla indicada.
    req.collection = db.collection(coleccion); 

    return next();
});

//Autorizacion
function auth(req, res, next){
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next(new Error("Falta el token."));
    }

    Token.decodificaToken(req.headers.authorization.split(" ")[1])
        .then(userId => {
            return next();
        }).catch(err => {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Autorización necesaria.'
            });
            return next(err);
        });
}

function asignarDatos(datos){
    var fecha = Date.now();
    var userData = {
        correo: datos.correo,
        username: datos.username,
        contraseña: datos.contraseña,
        ultimaConexion: moment(fecha).format('LLLL'),
        iniciado: false
    }

    return userData;
}

//Rutas y controladores
app.get('/api', (req, res, next) =>{

    db.getCollectionNames((err, colecciones) =>{
        if(err) return next(err);
        console.log(colecciones);

        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    });
});

app.get('/api/:colecciones', (req, res, next) =>{

    req.collection.find((err, coleccion) =>{
        if(err) return next(err);
        console.log(coleccion);

        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: coleccion
        })
    });
});

app.get('/api/:colecciones/:id', (req, res, next) =>{
    const idNum = req.params.id;
    const queColeccion = req.params.colecciones;

    req.collection.findOne( {_id: id(idNum)}, (err, elemento) =>{
        if(err) return next(err);
        console.log(elemento);

        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elementos: elemento
        })
    });
});

//FORMATO
//correo: X
//username: X
//contraseña: X
app.post('/api/users/registro', auth, (req, res, next) =>{
    var elemento = req.body;
    var userData = asignarDatos(elemento);

    db.collection('users').findOne({correo: userData.correo}, (err,usuarioExiste) =>{
        if(err) return next(err);
        if(!usuarioExiste){
            Password.encriptaPassword(userData.contraseña).then(hash =>{
                userData.contraseña = hash;
                db.collection('users').save(userData, (err, elemento) =>{
                    if(err) return next(err);
                    console.log(elemento);
                    res.status(201).json({
                        result: 'OK',
                        coleccion: 'users',
                        elementos: elemento
                    });
                });
            });
        }else{
            res.status(400).json({
                result: 'KO',
                mensajes: "Este email ya esta registrado."
            });
        }
    });
});

app.put('/api/users/login', auth, (req, res, next) =>{
    var datos = req.body;

    db.collection('users').findOne({correo: datos.correo}, (err, usr) =>{
        if(usr){
            Password.comparaPassword(datos.contraseña, usr.contraseña).then(comparacion =>{
                if(comparacion){

                    if(!usr.iniciado){

                        var token = Token.creaToken(usr);
                        var fechaAux = Date.now();
                        var act = {
                            ultimaConexion:moment(fechaAux).format('LLLL'),
                            iniciado: true
                        };

                        db.collection('users').update(
                            { _id: id(usr._id)},
                            { $set: act},
                            { safe: true, multi: false},
                            ( err, resultado ) =>{
                                if(err) return next(err);
                                console.log(resultado);
                                res.status(201).json({
                                    result: 'OK',
                                    mensajes: 'Sesion iniciada',
                                    token: token
                                });
                                
                            }
                        );
                    }else{
                        res.json({
                            result: 'OK',
                            mensajes: 'La sesion ya se encuentra iniciada'
                        });
                    }

                    
                }else{
                    res.status(400).json({
                        result: 'KO',
                        mensajes: 'La contraseña es incorrecta'
                    });
                }
            });
        }else{
            res.status(400).json({
                result: 'KO',
                mensajes: 'No existe este usuario'
            });
        }
    });
})

app.put('/api/users/logout', auth, (req, res, next) =>{
    var datos = req.body;

    db.collection('users').findOne({correo: datos.correo}, (err, usr) =>{
        if(usr){
            if(usr.iniciado){
                var token = Token.creaToken(usr);
                var act = {iniciado: false};

                db.collection('users').update(
                    { _id: id(usr._id)},
                    { $set: act},
                    { safe: true, multi: false},
                    ( err, resultado ) =>{
                        if(err) return next(err);
                        console.log(resultado);
                        res.status(201).json({
                            result: 'OK',
                            mensajes: 'Sesion cerrada',
                            token: token
                        });
                    }
                );
            }else{
                res.json({
                    result: 'KO',
                    mensajes: 'No hay ninguna sesion iniciada'
                });
            }
            
                
        }else{
            res.status(400).json({
                result: 'KO',
                mensajes: 'No existe este usuario'
            });
        }
    });
})

app.delete('/api/:colecciones/:id', auth, (req, res, next) =>{
    const idNum = req.params.id;
    const queColeccion = req.params.colecciones;

    req.collection.remove(
        { _id: id(idNum) },
        ( err, result ) =>{
            if(err) return next(err);
            console.log(result);

            res.json({
                result: 'OK',
                coleccion: queColeccion,
                _id: idNum,
                resultado: result
            });
        }
    );
});


https.createServer(opciones, app).listen(port, ()=>{
    console.log(`API REST CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

/*
app.listen(port, () => {
    console.log(`API REST CRUD ejecutandose en http://localhost:${port}/api/{colecciones}/{id}`);
});
*/
