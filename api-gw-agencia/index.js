'use strict'

const port = process.env.PORT || 4000;

const https = require('https');
const fs = require('fs');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

var URL_TRANS = "https://localhost:4600/";
var URL_USR = "https://localhost:4400/";

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}
const Token = require('./services/token.service');
const app = express();

//Declaramos nuestros middleWare
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}

app.get('/listarCoches', (req, res, next) =>{
    const queToken = req.headers.authorization.split(" ")[1];
    const queColeccion = req.params.colecciones;
    const queURL = `${URL_TRANS}listarCoches`;
    
    fetch( queURL , {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    })
        .then(res => res.json())
        .then(json => {
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elementos: json.elementos
            });
        });
});

app.post('/api/:colecciones/:accion', auth, (req, res, next) =>{
    const newElement = req.body;
    const queColeccion = req.params.colecciones;
    const queToken = req.params.token;
    const queAccion = req.params.accion;
    const queURL = `${urlValor(queColeccion)}/${queColeccion}/${queAccion}`;

    fetch( queURL, {
                        method: 'POST',
                        body: JSON.stringify(newElement),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`
                        }
                   })
        .then(res => res.json())
        .then(json => {
            res.json({
                json
            });
        });
});

app.post('/api/:colecciones', auth, (req, res, next) =>{
    const newElement = req.body;
    const queColeccion = req.params.colecciones;
    const queToken = req.params.token;
    const queURL = `${urlValor(queColeccion)}/${queColeccion}`;

    fetch( queURL, {
                        method: 'POST',
                        body: JSON.stringify(newElement),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`
                        }
                   })
        .then(res => res.json())
        .then(json => {
            res.json({
                json
            });
        });
});

app.put('/api/:colecciones/:id', auth, (req, res, next) =>{
    const elem = req.body;
    const queColeccion = req.params.colecciones;
    const idNum = req.params.id;
    const queURL = `${urlValor(queColeccion)}/${queColeccion}/${idNum}`;
    const queToken = req.params.token;

    fetch( queURL, {
                        method: 'PUT',
                        body: JSON.stringify(elem),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`
                        }
                   })
        .then(res => res.json())
        .then(json => {
            res.json({
                json
            });
        });
});

app.delete('/api/:colecciones/:id', auth, (req, res, next) =>{
    const idNum = req.params.id;
    const queColeccion = req.params.colecciones;
    const queURL = `${urlValor(queColeccion)}/${queColeccion}/${idNum}`;
    const queToken = req.params.token;

    fetch( queURL, {
                        method: 'DELETE',
                        headers: {
                            'Authorization': `Bearer ${queToken}`
                        }
                    })
        .then(res => res.json())
        .then(json =>{
            res.json({
                json
            });
        });
});

https.createServer(opciones, app).listen(port, ()=>{
    console.log(`API REST CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

/*
app.listen(port, () => {
    console.log(`API REST CRUD ejecutandose en http://localhost:${port}/api/{colecciones}/{id}`);
});
*/