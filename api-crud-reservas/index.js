'use strict'

const port = process.env.PORT || 4500;

const https = require('https');
const fs = require('fs');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const { allowedNodeEnvironmentFlags } = require('process');

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const URL_DB = "mongodb+srv://dmg102:BYEfqRO1gq4Aeh7F@sd.c6nun.mongodb.net/RESERVAS?retryWrites=true&w=majority";
const Token = require('./services/token.service');
const app = express();

var db = mongojs(URL_DB); //Enlace con la DB
var id = mongojs.ObjectID; //Convertir un id textual en objeto mongojs

//Declaramos nuestros middleWare
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) =>{
    console.log('param/api/:colecciones');
    console.log('coleccion: ', coleccion);

    //Creamos puntero a una funcion que apunta a la DB y tabla indicada.
    req.collection = db.collection(coleccion); 

    return next();
});

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}

//Rutas y controladores
app.get('/listarRE', (req, res, next) =>{

    req.collection.find((err, coleccion) =>{
        if(err) return next(err);
        console.log(coleccion);

        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: coleccion
        })
    });
});

app.get('/reservas/:id', (req, res, next) =>{
    const idNum = req.params.id;
    const queColeccion = req.params.colecciones;

    req.collection.findOne( {_id: id(idNum)}, (err, elemento) =>{
        if(err) return next(err);
        console.log(elemento);

        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elementos: elemento
        })
    });
});

//FORMATO
//Usuario: X
//IDprod: X,X...
//Precio: X
app.post('/reservas', auth, (req, res, next) =>{
    const newElement = req.body;
    const queID = newElement.IDprod;
    const tipo = newElement.tipo;
    

    req.collection.save(newElement, (err, elementoGuardado) =>{
        if(err) return next(err);

        res.status(201).json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elemento: elementoGuardado
        });
    });
});

app.put('/reservas/:id', auth, (req, res, next) =>{
    const idNum = req.params.id;
    const queColeccion = req.params.colecciones;
    const elem = req.body;

    req.collection.update(
        { _id: id(idNum) },
        { $set: elem },
        { safe: true, multi: false },
        ( err, result ) =>{
            if(err) return next(err);
            console.log(result);

            res.json({
                result: 'OK',
                coleccion: queColeccion,
                _id: idNum,
                resultado: result
            });
        }
    );
});

app.delete('/reservas/:id', auth, (req, res, next) =>{
    const idNum = req.params.id;
    const queColeccion = req.params.colecciones;

    req.collection.remove(
        { _id: id(idNum) },
        ( err, result ) =>{
            if(err) return next(err);
            console.log(result);

            res.json({
                result: 'OK',
                coleccion: queColeccion,
                _id: idNum,
                resultado: result
            });
        }
    );
});

https.createServer(opciones, app).listen(port, ()=>{
    console.log(`API REST CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

/*
app.listen(port, () => {
    console.log(`API REST CRUD ejecutandose en http://localhost:${port}/api/{colecciones}/{id}`);
});
*/