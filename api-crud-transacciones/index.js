'use strict'

const port = process.env.PORT || 4600;

const https = require('https');
const fs = require('fs');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const fetch = require('node-fetch');
const { allowedNodeEnvironmentFlags } = require('process');

const PAGOS_URL = "https://localhost:4600/";
const RESERVAS_URL = "https://localhost:4500/";
const COCHES_URL = "https://localhost:4300/";
const AVIONES_URL = "https://localhost:4100/";
const HOTELES_URL = "https://localhost:4200/";ç

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const Token = require('./services/token.service');

//const URL_DB = "mongodb+srv://dmg102:BYEfqRO1gq4Aeh7F@sd.c6nun.mongodb.net/RESERVAS?retryWrites=true&w=majority";

const app = express();

//var db = mongojs(URL_DB); //Enlace con la DB
//var id = mongojs.ObjectID; //Convertir un id textual en objeto mongojs

//Declaramos nuestros middleWare
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) =>{
    console.log('param/api/:colecciones');
    console.log('coleccion: ', coleccion);

    //Creamos puntero a una funcion que apunta a la DB y tabla indicada.
    req.collection = db.collection(coleccion); 

    return next();
});

//Autorizacion
function auth(req, res, next){
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: "No has enviado el token en la cabecera."
        });
        return next(new Error("Falta el token."));
    }

    Token.decodificaToken(req.headers.authorization.split(" ")[1])
        .then(userId => {
            return next();
        }).catch(err => {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Autorización necesaria.'
            });
            return next(err);
        });
}

//Rutas y controladores
app.get('/listarAviones', (req, res, next) =>{
    const queToken = req.headers.authorization.split(" ")[1];
    const url = `${AVIONES_URL}listarAV`;

    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    })
    .then(resp=>resp.json())
    .then(json=>{
        res.json({
            result: 'OK',
            colecciones:'aviones',
            elementos: json.elementos
        });
    });
});

app.get('/listarCoches', (req, res, next) =>{
    const queToken = req.headers.authorization.split(" ")[1];
    const url = `${COCHES_URL}listarCO`;

    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    })
    .then(resp=>resp.json())
    .then(json=>{
        res.json({
            result: 'OK',
            colecciones:'coches',
            elementos: json.elementos
        });
    });
});

app.get('/listarHoteles', (req, res, next) =>{
    const queToken = req.headers.authorization.split(" ")[1];
    const url = `${HOTELES_URL}listarHO`;

    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    })
    .then(resp=>resp.json())
    .then(json=>{
        res.json({
            result: 'OK',
            colecciones:'hoteles',
            elementos: json.elementos
        });
    });
});

app.post('/reservarAvion', auth, (req, res, next) =>{

    const queToken = req.headers.authorization.split(" ")[1];
    const id = req.params.id;
    const url = `${AVIONES_URL}aviones/${id}`;

    Token.decodificaToken(queToken).then(userId =>{
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${queToken}`
            }
        })
        .then(resp=>resp.json())
        .then(json=>{
            if(json.elementos.reservado){
                res.status(400).json({
                    result: 'KO',
                    mensajes: 'El avion ya se encuentra reservado'
                });
            }

            fetch(url, {
                method: 'PUT',
                body: JSON.stringify({reservado:true}),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`
                }
            });

            if((Math.random()*5)==0){
                fetch(url, {
                    method: 'PUT',
                    body: JSON.stringify({reservardo:false}),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`
                    }
                });

                res.status(400).json({
                    result: 'KO',
                    mensajes: 'Pago incorrecto'
                });
            }

            fetch(`${PAGOS_URL}pagos`, {
                method:'POST',
                body: JSON.stringify({usuario:userId, precio:json.elementos.precio}),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Bearer ${queToken}`
                }
            });

            fetch(`${RESERVAS_URL}reservas`, {
                method: 'POST',
                body: JSON.stringify({usuario:userId, IDprod:[{avion:id}], precio:json.elementos.precio}),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Bearer ${queToken}`
                }
            });

            res.status(200).json({
                result:'OK',
                mensajes: 'Reserva realizada correctamente'
            });

        }).catch(err =>{
            res.status(400).json({
                result: 'KO',
                mensajes: 'Id no encontrada'
            })
        })
    }).catch(err =>{
        res.status(401).json({
            result: 'KO',
            mensajes: 'Autorización necesaria'
        });
    });
});

app.post('/reservarCoche', auth, (req, res, next) =>{
    
});

app.post('/reservarHotel', auth, (req, res, next) =>{
    
});

https.createServer(opciones, app).listen(port, ()=>{
    console.log(`API REST CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

/*
app.listen(port, () => {
    console.log(`API REST CRUD ejecutandose en http://localhost:${port}/api/{colecciones}/{id}`);
});
*/